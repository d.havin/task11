// import { validationResult } from "express-validator";
import { Request, Response } from 'express'
const expressValidator = require ('express-validator')
const validationResult = expressValidator.validationResult()
import { User, IUser } from "../models/user";

interface CustomRequest<T>extends Request {
    body: T
} 

const createUser = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const errors = validationResult(request)
        if (!errors.isEmpty()) {
            let err = errors;
            return response.status(400).send(` error message: ${err.errors[0].msg}`)
        }
        let user = await User.findOne({ email: request.body.email });
        if (user) {
            return response.status(400).send(`Such user is already existed`)
        }
        const newUser = new User(request.body);
        await newUser.save();
        response.send(`Create new user:
                        user name: ${newUser.name}
                        user surname: ${newUser.surname}
                        email: ${newUser.email}`);
    } catch (err) {
        response.status(500).send(`Something went wrong`)
    }

}

const deleteUser = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        let user = await User.findOne({ _id: request.params.id });
        if (!user) {
            return response.send(`Can't find this user.`);
        }
        User.deleteOne({ _id: request.params.id });
        response.send(`Delete user ${user.name} ${user.surname}`);
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't delete this user.`)
    }
}

const updateUser = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        let user = await User.findOne({ _id: request.params.id });
        if (!user) {
            return response.status(400).send(`Can't find this user`);
        }
        if (request.user.role == "admin" || request.user._id == request.params.id) {
            User.updateOne({ _id: request.params.id }, { surname: request.body.surname }, { new: true }, function (err, result) {
                if (err) return response.status(400).send(`Can't find this user.`)
            });
            return response.send(`Update user ${user.name} ${user.surname}`);
        }
        response.send(`It's not your profile. You don't have access yo update it`);
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't update this user.`)
    }
}

const getUserById = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        let user = await User.findOne({ _id: request.params.id });
        if (!user) {
            return response.send(`Can't find such user`)
        }
        response.send(`user name: ${user.name}
                        user surname: ${user.surname}
                        email: ${user.email}`)
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't see the user by id.`)
    }
}

const getAllUsers = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        let users = await User.find({ role: "user" });
        if (!users) {
            return response.status(400).send(`Can't find such users`)
        }
        response.send(`${users}`)
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't see all users.`)
    }
}

export {
    createUser,
    deleteUser,
    updateUser,
    getAllUsers,
    getUserById
}