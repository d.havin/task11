import { Request, Response} from 'express';
import { Cart, ICart } from '../models/cart';
import { User, IUser } from '../models/user';
import { Product } from '../models/product';

interface CustomRequest<T> extends Request {
    body: T,
} 

const createCart = async function (request: CustomRequest<ICart>, response: Response) {
    try {
        let cart = await Cart.findOne({ name: request.body.name });
        let user = await User.findOne({ _id: request.user._id });
        if (cart) {
            return response.status(400).send(`Such cart is already existed`)
        }
        if (user) {
            if (user.cart) {
                return response.status(400).send(`One user can't have more than one cart.`)
            }
            const newCart = await new Cart({ name: request.body.name });

            newCart.user = request.user._id;
            await newCart.save();

            user.cart = newCart._id;
            await user.save();
            response.send(`Create new cart:
                    cart name: ${newCart.name}`);
        }
    } catch (err) {
        response.status(500).send(`Something wents wrong. Can't add products to cart.`)
    }
}

const addProductToCart = async function (request: CustomRequest<ICart>, response: Response) {
    try {
        let userOfCart = await User.findOne({ _id: request.user._id });
        if (userOfCart) {
            let cart = await Cart.findOne({ _id: userOfCart.cart._id });
            if (!cart) {
                return response.status(400).send(`This user don't have a cart.`);
            } else {
                let product = await Product.findOne({ name: request.body.name });
                if (!product) {
                    return response.status(400).send(`Can't find such product.`)
                } else {
                    cart.products.push(product._id.toString());
                    await cart.save();
                    response.send(`Product ${product.name}, price ${product.price}$ added to cart`)
                }
            }
        }
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't add products to cart.`)
    }
}

const getCartById = async function (request: CustomRequest<ICart>, response: Response) {
    try {
        let cart = await Cart.findOne({ _id: request.params.id });
        if (!cart) {
            return response.status(400).send(`Can't find such cart`)
        }
        response.send(`Cart ${cart.name} with products ${cart.products}`)
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't see the cart by id.`)
    }
}

export {
    createCart,
    addProductToCart,
    getCartById
}