import { Category, ICategory } from '../models/category';
import { Request, Response} from 'express';

interface CustomRequest<T>extends Request {
    body: T,
} 

const createCategory = async function (request: CustomRequest<ICategory>, response: Response) {
    try {
        let category = await Category.findOne({ name: request.body.name });
        if (category) {
            return response.status(400).send(`Such category is already existed`)
        }
        const newCategory = new Category({ name: request.body.name });
        await newCategory.save();
        response.send(`Create new category:
                        category name: ${newCategory.name}`);
    } catch (err) {
        response.status(500).send(`Something went wrong. Can't create category.`)
    }
}

const deleteCategory = async function (request: CustomRequest<ICategory>, response: Response) {
    try {
        let category = await Category.findOne({ _id: request.params.id });
        if (!category) {
            return response.status(400).send(`Can't find this category.`);
        }
        Category.deleteOne({ _id: request.params.id }, function (err) {
            if (err) return response.status(400).send(`Can't find this category.`)
        });
        response.send(`Delete category ${category.name}`);
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't delete this category.`)
    }
}

const updateCategory = async function (request: CustomRequest<ICategory>, response: Response) {
    try {
        let category = await Category.findOne({ _id: request.params.id });
        if (!category) {
            return response.status(400).send(`Can't find this category.`);
        }
        Category.updateOne({ _id: request.params.id }, { name: request.body.name }, { new: true }, function (err, result) {
            if (err) return response.status(400).send(`Can't find this category.`)
        });
        response.send(`Update category ${category.name}`);
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't update this category.`)
    }
}

const getCategoryById = async function (request: CustomRequest<ICategory>, response: Response) {
    try {
        let category = await Category.findOne({ _id: request.params.id });
        if (!category) {
            return response.status(400).send(`Can't find this category.`);
        }
        response.send(`Category name: ${category.name}`);
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't see the category by id.`)
    }
}

const getAllCategories = async function (response: Response) {
    try {
        let categories = await Category.find();
        if (!categories) {
            return response.status(400).send(`Can't find categories.`);
        }
        response.send(`${categories}`)
    }
    catch (err) {
        response.status(400).send(`Something wents wrong. Can't see all categories.`)
    }
}

export {
    createCategory,
    deleteCategory,
    updateCategory,
    getCategoryById,
    getAllCategories
}