import { Category } from '../models/category';
import { Product, IProduct } from '../models/product';
import { Request, Response} from 'express';

interface CustomRequest<T> extends Request {
    body: T
} 

const createProduct = async function (request: CustomRequest<IProduct>, response: Response) {
    try {
        let product = await Product.findOne({ name: request.body.name, price: request.body.price });
        let category = await Category.findOne({ name: request.body.category });
        if (product) {
            return response.status(400).send(`Such product is already existed`)
        }
        if (!category) {
            return response.status(400).send(`Can't find such category. Change it or create new one`)
        }
        const newProduct = new Product({ name: request.body.name, price: request.body.price, category: request.body.category }); //!
        await newProduct.save();
        category.products.push(newProduct._id);
        await category.save();
        response.send(`Create new product:
                        product name: ${newProduct.name}
                        product price: ${newProduct.price}$`);
    } catch (err) {
        response.status(500).send(`Something went wrong. Can't create this product.`)
    }
}

const deleteProduct = async function (request: CustomRequest<IProduct>, response: Response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (product){
        let category = await Category.findOne({ name: product.category });
        if (category){
            for (let i = 0; i < category.products.length; i++) {
                if (category.products[i]._id.toString() == product._id.toString()) {
                    category.products.splice(category.products.indexOf(i), 1);
                    await category.save()
                }
                response.send(`Delete product ${product.name}`);
            }}}
        if (!product) {
            return response.send(`Can't find this product.`);
        }
        Product.deleteOne({ _id: request.params.id });
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't delete this product.`)
    }
}

const updateProduct = async function (request: CustomRequest<IProduct>, response: Response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (!product) {
            return response.status(400).send(`Can't find this product`);
        }
        Product.updateOne({ _id: request.params.id }, { price: request.body.price }, { new: true }, function (err, result) {
            if (err) return console.log(err);
        });
        response.send(`Update product ${product.name}`);
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't update this product.`)
    }
}

const getProductById = async function (request: CustomRequest<IProduct>, response: Response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (!product) {
            return response.status(400).send(`Can't find this product`);
        }
        response.send(`Product name: ${product.name}, price: ${product.price}$`)
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't see the product by id.`)
    }
}

const getAllProducts = async function (response: Response) {
    try {
        let products = await Product.find();
        if (!products) {
            return response.status(400).send(`Can't get all products`)
        }
        response.send(`${products}`)
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't see all products.`)
    }
}

export {
    createProduct,
    deleteProduct,
    updateProduct,
    getProductById,
    getAllProducts
}