import { Cart } from '../models/cart';
import { User } from '../models/user';
import { Order, IOrder } from '../models/order';
import { Request, Response } from 'express'

interface CustomRequest<T> extends Request {
    body: T,
}

const createOrder = async function (request: CustomRequest<IOrder>, response: Response) {
    try {
        let user = await User.findOne({ _id: request.user._id });
        let cart = await Cart.findOne({ _id: request.params.id });
        let order = await Order.findOne({ number: request.body.number });
        if (!cart) {
            return response.status(400).send(`Can't find cart with such id.`)
        }
        if (cart.products.length == 0) {
            return response.status(400).send(`You need add products to cart first.`)
        }
        if (order) {
            return response.status(400).send(`Order with such number is aleady exist. Please change a number.`)
        }
        const newOrder = new Order({ number: request.body.number });
        newOrder.user = request.user._id;
        await newOrder.save();

        newOrder.products = cart.products;
        await newOrder.save();
        if (user) {
            user.orders.push(newOrder._id);
            await user.save();
            await Cart.deleteOne({ _id: request.params.id });

            response.send(`New order with number ${newOrder.number} was successfully created.`);
        }


    } catch (err) {
        response.status(500).send(`Something went wrong. Can't create this order.`)
    }

}

const deleteOrder = async function (request: CustomRequest<IOrder>, response: Response) {
    try {
        let order = await Order.findOne({ _id: request.params.id });
        let user = await User.findOne({ _id: request.user._id });
        if (!order) {
            return response.status(400).send(`Can't find this order.`)
        }

        if (user) {
            if (user.role == "user" && order.user._id.toString() !== request.user._id.toString()) {
                return response.status(400).send(`It's not your order and you don't have access to delete this order.`)
            }
            Order.deleteOne({ _id: request.params.id });
            for (let i = 0; i < user.orders.length; i++) {
                if (user.orders[i]._id.toString() == order._id.toString()) {
                    user.orders.splice(user.orders.indexOf(i), 1);
                    await user.save();
                }
            }
            response.send(`Delete order ${order.number}`);
        }


    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't find this order.`)
    }
}

const getAllOrders = async function (request: CustomRequest<IOrder>, response: Response) {
    try {
        let orders = await Order.find();
        let user = await User.findOne({ _id: request.user._id });
        if (!orders) {
            return response.status(400).send(`Orders are not found`)
        }
        if (user) {
            if (user.role == "admin") {
                response.send(`Orders ${orders}`);
            }

            else if (user.role == "user") {
                response.send(`Your orders: ${user.orders}`)
            }
        }


    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't find this order.`)
    }
}

const getOrderById = async function (request: CustomRequest<IOrder>, response: Response) {
    try {
        let order = await Order.findOne({ _id: request.params.id });
        if (!order) {
            return response.status(400).send(`Orders are not found`)
        }
        response.send(`Order number: ${order.number}`)
    }
    catch (err) {
        response.status(500).send(`Something went wrong. Can't find the order by id.`)
    }
}

export {
    createOrder,
    deleteOrder,
    getAllOrders,
    getOrderById
}