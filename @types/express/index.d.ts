import { User} from "../../models/user";
import { Cart} from "../../models/cart";
import { Product} from "../../models/product";

declare global{
    namespace Express {
        interface Request {
            product: Product
        }
    }
}

declare global{
    namespace Express {
        interface Request {
            user: User
        }
    }
}

declare global{
    namespace Express {
        interface Request {
            cart: Cart
        }
    }
}
