const express = require ('express')
const expressValidator = require ('express-validator')
const check = expressValidator.check()
// import { check } from ('express-validator'); 

import { createUser, deleteUser, updateUser, getUserById, getAllUsers } from '../controllers/userController';
import { checkAccess } from '../controllers/authController';

const userRouter = express.Router();

userRouter.post("/create",[
    check('email', "email field cannot be empty").notEmpty(),
    check('password', "password length must be between 4 and 20 symbols").isLength({min:4, max: 20})
], checkAccess, createUser);
userRouter.delete("/:id", checkAccess, deleteUser);
userRouter.put("/:id", updateUser);
userRouter.get("/allUsers", getAllUsers);
userRouter.get("/:id", getUserById);

export default userRouter;