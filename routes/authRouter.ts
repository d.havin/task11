import express from 'express';
import { registration, authorization, createGoogleUrl, getToken } from '../controllers/authController';
import { check } from 'express-validator';  

const authRouter = express.Router();

authRouter.post("/registration", [
    check('email', "email field cannot be empty").notEmpty(),
    check('password', "password length must be between 4 and 20 symbols").isLength({min:4, max: 20})
], registration);
authRouter.post("/login", authorization);
authRouter.get("/google", createGoogleUrl);
authRouter.get("/google/gettoken", getToken );

export default authRouter;
 