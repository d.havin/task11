import authRouter from "./authRouter";
import userRouter from './userRouter'
import categoryRouter from "./categoryRouter";
import productRouter from "./productRouter";
import cartRouter from "./cartRouter";
import orderRouter from './orderRouter'

export {
    authRouter,
    userRouter,
    categoryRouter,
    productRouter,
    cartRouter,
    orderRouter
}