// import * as express from 'express'
// import * as mongoose from 'mongoose'
// import  json from 'body-parser';
const express = require ('express')
const mongoose = require ('mongoose')
import * as bodyParser from 'body-parser';
import { authRouter, userRouter, categoryRouter} from './routes/index'; //cartRouter, productRouter, orderRouter 
import { authenticateJWT } from './controllers/authController'

const app = express()
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));
const PORT = 8080

const url = 'mongodb://localhost:27017/shop'
mongoose.connect(url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
}, () => {
    console.log('connected to database')
})

app.use("/auth", authRouter);
app.use("/user", authenticateJWT, userRouter)
app.use("/category", authenticateJWT, categoryRouter);
// app.use("/product", authenticateJWT, productRouter);
// app.use("/cart", authenticateJWT, cartRouter);
// app.use("/order", authenticateJWT, orderRouter)

app.listen(PORT, () => console.log(`app running on port ${PORT}`)) 