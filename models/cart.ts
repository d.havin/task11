import * as mongoose from 'mongoose';
const autopopulate =require ('mongoose-autopopulate')
import { IProduct } from '../models/product';

const Schema = mongoose.Schema;

interface ICart extends mongoose.Document {
    name: string,
    user: string,
    products: Array<IProduct | any>
    _id: string    // надо ли??
}

const cartSchema = new Schema({
    name: String,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: true 
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "product",
        autopopulate: true 
    }]
},
    { versionKey: false }
);

cartSchema.plugin(autopopulate);
const Cart = mongoose.model<ICart>('cart', cartSchema);

export { cartSchema, Cart, ICart};