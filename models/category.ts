import * as mongoose from 'mongoose';
const autopopulate = require ('mongoose-autopopulate')
const Schema = mongoose.Schema;
import { IProduct } from '../models/product.js';

interface ICategory extends mongoose.Document {
    name: string,
    products: Array<IProduct | any>
}

const categorySchema = new Schema({
    name: {
        type: String,
        default: 'newCategory'
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "product",
        autopopulate: true 
    }]
},
    { versionKey: false }
);

categorySchema.plugin(autopopulate)
const Category = mongoose.model<ICategory>('category', categorySchema);

export { categorySchema, Category, ICategory };