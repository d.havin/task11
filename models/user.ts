import * as mongoose from 'mongoose';
const autopopulate =require ('mongoose-autopopulate')
const Schema  = mongoose.Schema; 
import { IOrder } from '../models/order';

interface IUser extends mongoose.Document {
   name: string,
   surname: string,
   email: string,
   password: string,
   role: string,
   cart: string, 
   orders: Array<IOrder>
}

const userSchema = new Schema({
    name: {
        type: String,
        require: true,     
        default: "newUser"
    },
    surname: {
        type: String,
        require: true,      
        default: "surname"
    },
    email: {
        type: String,
        require: true,      
        unique: true       
    },
    password: {
        type: String,
        require: true
    },
    role: {
        type: String,
        default: 'user'
    },
    cart: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "cart",
        autopopulate: true 
    },
    orders: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "order",
        autopopulate: true 
    }]
},
    { versionKey: false });

userSchema.plugin(autopopulate)
const User = mongoose.model<IUser>('user', userSchema);

export { userSchema, User, IUser };
