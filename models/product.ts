import * as mongoose from 'mongoose';
const autopopulate = require ('mongoose-autopopulate')
const Schema  = mongoose.Schema;

interface IProduct extends mongoose.Document {
    name: string,
    price: number,
    category: string,
}

const productSchema = new Schema({
    name: {
        type: String,
        default: "newProduct"
    },
    price: {
        type: Number, 
        default: 5   
    },
    category: {
        type: String,
        default: "newCategory",
        require: true
    }
},
    { versionKey: false }
);

productSchema.plugin(autopopulate);
const Product = mongoose.model<IProduct>('product', productSchema);

export { productSchema, Product, IProduct};