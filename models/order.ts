import * as mongoose from 'mongoose';
const autopopulate = require ('mongoose-autopopulate')
const Schema = mongoose.Schema;
import { IProduct } from '../models/product';

interface IOrder extends mongoose.Document {
    number: number,
    user: number,
    products: Array<IProduct | any>
}

const orderSchema = new Schema({
    number:{
        type: Number,
        default: 111
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: true 
    },
    products:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: "product",
        autopopulate: true 
    }]
},
    { versionKey: false }
);

orderSchema.plugin(autopopulate);
const Order = mongoose.model<IOrder>('order', orderSchema);

export { orderSchema, Order, IOrder };